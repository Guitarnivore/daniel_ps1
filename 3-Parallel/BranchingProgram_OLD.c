/*
Copyright 2019 Daniel Lansdaal

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

int main (int argc, char ** argv) {

	if (argc == 3) {
	
		int myRank;
		int numProcesses;
	
		MPI_Init(&argc, &argv);
		MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
		MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
		
		fprintf(stderr, "MPI Initiated...");
		
	
		//read in all n, m, k, and r
		int n;		//n variables?
		int m;		//number of rules in branching program
		int k;		//length of the multiplication table (like mod 3)
		int r;		//number of elements in final set
		
		FILE * bp = fopen(argv[1], "r");
		int ok = fscanf(bp, "%i\n", &n);
		ok = fscanf(bp, "%i\n", &m);
		ok = fscanf(bp, "%i\n", &k);
		ok = fscanf(bp, "%i\n", &r);
		
		if (!ok) {
			fprintf(stderr, "uh oh");
		}
		
		
		//read in mult table
		int multTable[k][3];
		int i;
		for (i = 0; i < k; i++) {
			ok = fscanf(bp, "%i %i %i\n", &multTable[i][0], &multTable[i][1], &multTable[i][2]);
			if (!ok) {
				fprintf(stderr, "bad");
			}
		}
		
		int finalSet[r];		
		int length = strlen(argv[2]);
		int input[length];
		//read in final set (just rank 0?)
		if (myRank == 0) {
			for (i = 0; i < r; i++) {
				ok = fscanf(bp, "%i\n", &finalSet[i]);
				if (!ok) {
					fprintf(stderr, "uh oh");	
				}
			}
		}
		//read in input string
		for (i = 0; i < length; i++) {
			input[i] = argv[2][i] - '0';
		}
		
		fprintf(stderr, "All files read...");
		
		//so read in bp first and then after that scatter, and do transformations
		//do calculations based on mult table
		
		int branchingProgram[m*3];
		
		fprintf(stderr, "arraycreated");
		for (i = 0; i < m*3; i+=3) {
			ok = fscanf(bp, "%i %i %i\n", &branchingProgram[i], &branchingProgram[i+1], &branchingProgram[i+2]);
			if (!ok) {
				fprintf(stderr, "uh oh");
			}
		}
		fclose(bp);
		//assume evenly divisible
		int sendCount = (m / numProcesses) * 3;
		int subArray[sendCount];
		
		MPI_Scatter(branchingProgram, sendCount, MPI_INT, subArray, sendCount, MPI_INT, 0, MPI_COMM_WORLD);
		
		
		
		fprintf(stderr, "Scattered...");
		
		int newArray[m / numProcesses];
		int count = 0;
		
		for (i = 0; i < sendCount; i+=3) {
			int index = subArray[i];
			if (input[index] == 0) {
				newArray[count] = subArray[i+1];
			}
			else if (input[index] == 1) {
				newArray[count] = subArray[i+2];
			}
			count++;
		}
		
		
		fprintf(stderr, "Transformed...");
		
		//do calculations based on mult table
		int j;
		for (i = 1; i < count; i++) {
			for (j = 0; j < k; j++) {
				if (newArray[i-1] == multTable[j][0] && newArray[i] == multTable[j][1]) {
					newArray[i] = multTable[j][2];
					break;
				}
			}
		}
		
		fprintf(stderr, "Mult table done...");
		
		int result[numProcesses];
		//gather
		MPI_Gather(&newArray[count-1], 1, MPI_INT, result, 1, MPI_INT, 0, MPI_COMM_WORLD);
		
		fprintf(stderr, "Gathered...");
		
		//mult table
		if (myRank == 0) {
			for (i = 1; i < numProcesses; i++) {
				for (j = 0; j < k; j++) {
					if (result[i-1] == multTable[j][0] && result[i] == multTable[j][1]) {
						result[i] = multTable[j][2];
						break;
					}
				}
			}
			int isFinal = 0;
			for (i = 0; i < r; i++) {
				if (result[numProcesses-1] == finalSet[i]) {
					isFinal = 1;
					break;
				}
			}
			fprintf(stderr, "%i", result[numProcesses-1]);
			if (isFinal) {
				printf("\n\n1\n");
			}
			else {
				printf("\n\n0\n");
			}
		}
		MPI_Finalize();
		
	}
	
	
}
