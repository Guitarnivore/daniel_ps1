/*
Copyright 2019 Daniel Lansdaal

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>

int isEvenPermutation(int a, int b, int c, int d, int e) {
	
	int count = 0;
	if (a > b) {
		count++;
	}
	if (a > c) {
		count++;
	}
	if (a > d) {
		count++;
	}
	if (a > e) {
		count++;
	}
	if (b > c) {
		count++;
	}
	if (b > d) {
		count++;
	}
	if (b > e) {
		count++;
	}
	if (c > d) {
		count++;
	}
	if (c > e) {
		count++;
	}
	if (d > e) {
		count++;
	}
	
	if (count % 2 == 0) {
		return 1;
	}
	else {
		return 0;
	}
}

int * multiply(int* a, int* b) {
	static int result[5];
	
	int i;
	for (i = 0; i < 5; i++) {
		result[i] = b[a[i]-1];
		//fprintf(stderr, "%i ", result[i]);
	}
	//fprintf(stderr, "\n");
	
	return result;
}

int main (int argc, char ** argv) {
	int a;
	int b;
	int c;
	int d;
	int e;
	
	
	
	int A[60][5];
	int count = 0;
	for (a = 1; a <= 5; a++) {
		for (b = 1; b <= 5; b++) {
			for (c = 1; c <= 5; c++) {
				for (d = 1; d <= 5; d++) {
					for (e = 1; e <= 5; e++) {
						if (a != b && a != c && a != d && a != e && b != c && b != d && b != e && c != d && c != e && d != e) {
							int result = isEvenPermutation(a, b, c, d, e);
							if (result) {
								A[count][0] = a;
								A[count][1] = b;
								A[count][2] = c;
								A[count][3] = d;
								A[count][4] = e;
								count++;
							}
						}
					}
				}
			}
		}
	}
	
	
	
	FILE * output = fopen("a5.txt", "w");
	if (output) {
	}
	int i;
	int j;
	int k;
	
	for (i = 0; i < 60; i++) {
		for (j = 0; j < 60; j++) {
			for (k = 0; k < 60; k++) {
				int * result = multiply(A[i], A[j]);
				if (result[0] == A[k][0] && result[1] == A[k][1] && result[2] == A[k][2] && result[3] == A[k][3] && result[4] == A[k][4]) {
					//fprintf(stderr, "we in");
					//print i j k
					int ok = fprintf(output, "%i %i %i\n", i, j, k);
					if (ok) {
					}
				}
			}
		}
	}
	fclose(output);
}


