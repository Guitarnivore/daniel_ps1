/*
Copyright 2019 Daniel Lansdaal

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char ** argv) {

	if (argc == 3) {

		int n;		//n variables?
		int m;		//number of rules in branching program
		int k;		//length of the multiplication table (like mod 3)
		int r;		//number of elements in final set
		
		FILE * bp = fopen(argv[1], "r");
		
		fscanf(bp, "%i\n", &n);
		fscanf(bp, "%i\n", &m);
		fscanf(bp, "%i\n", &k);
		fscanf(bp, "%i\n", &r);
		
		char multTable[k][3];
		int i;
		for (i = 0; i < k; i++) {
			fscanf(bp, "%c %c %c\n", &multTable[i][0], &multTable[i][1], &multTable[i][2]);
		}
		
		char finalSet[r];
		for (i = 0; i < r; i++) {
			fscanf(bp, "%c\n", &finalSet[i]);
		}
		
		char out[m];
		int length = strlen(argv[2]);
		char input[length];
		for (i = 0; i < length; i++) {
			input[i] = argv[2][i];
		}
		for (i = 0; i < m; i++) {
			int index;
			char ifZero;
			char ifOne;
			fscanf(bp, "%i %c %c\n", &index, &ifZero, &ifOne);
			
			if (input[index] == '0') {
				out[i] = ifZero;
			}
			else if (input[index] == '1') {
				out[i] = ifOne;
			}
			else {
				fprintf(stderr, "Houston, we have a problem");
			}
		}
		fclose(bp);
		
		
		int j;
		for (i = 1; i < m; i++) {
			for (j = 0; j < k; j++) {
				if (out[i-1] == multTable[j][0] && out[i] == multTable[j][1]) {
					out[i] = multTable[j][2];
					break;
				}
			}
		}
		
		int isFinal = 0;
		for (i = 0; i < r; i++) {
			if (out[m-1] == finalSet[i]) {
				isFinal = 1;
				break;
			}
		}
		
		if (isFinal) {
			printf("1\n");
		}
		else {
			printf("0\n");
		}
	
	}

}
